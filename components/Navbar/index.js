import Cookies from "js-cookie";
import Link from "next/link";
import Router from "next/router";
import React from "react";
import { Moon, Sunny } from "react-ionicons";
import { useDispatch, useSelector } from "react-redux";
import { changeMode } from "../../redux/reducer/mode";
import { useRouter } from "next/router";

const Navbar = ({ token }) => {
  const mode = useSelector((state) => state.themeMode.value);
  const user = useSelector((state) => state.userData.value);
  const router = useRouter();
  const dispatch = useDispatch();

  const handlerHome = () => {
    if (token) {
      Router.push("/home");
    } else {
      Router.push("/log-in");
    }
  };

  const handlerLoginLogout = () => {
    if (token) {
      Cookies.remove("token");
      Router.push("/log-in");
      return;
    }
  };

  return (
    <div
      className={`${
        mode ? "shadow-lg shadow-white" : "shadow-lg shadow-black"
      } navbar bg-transparent mb-20`}
    >
      <div className=" container mx-auto flex gap-5">
        <div className="flex-none lg:flex-1">
          <button
            type="button"
            onClick={handlerHome}
            className="text-black btn btn-lg btn-ghost text-base px-5 dark:text-white hover:bg-blue-600"
          >
            Home
          </button>
        </div>
        <div className=" flex-none ">
          <div className="menu menu-horizontal p-0">
            <button className="text-black text-base btn btn-lg btn-ghost px-5 dark:text-white hover:bg-blue-600">
              <Link href={"/"}>Game List</Link>
            </button>
            {token ? (
              <button className="text-black text-base btn btn-lg btn-ghost px-5 dark:text-white hover:bg-blue-600">
                <Link href={"/profile"}>{user}</Link>
              </button>
            ) : (
              <button
                type="button"
                className=" text-black text-base btn btn-lg btn-ghost px-5 dark:text-white hover:bg-blue-600"
              >
                <Link href={"/register"}>Sign Up</Link>
              </button>
            )}
            {token ? (
              <button
                type="button"
                onClick={handlerLoginLogout}
                className=" text-black text-base btn btn-lg btn-ghost px-5 dark:text-white hover:bg-blue-600"
              >
                Logout
              </button>
            ) : (
              <button
                type="button"
                className=" text-black text-base btn btn-lg btn-ghost px-5 dark:text-white hover:bg-blue-600"
              >
                <Link href={"/log-in"}>Login</Link>
              </button>
            )}
            <button
              className="hover:pointer"
              onClick={() => dispatch(changeMode())}
            >
              {mode ? (
                <>
                  <Moon
                    color={"#ffae00"}
                    title={"Light Mode"}
                    height="30px"
                    width="30px"
                  />
                </>
              ) : (
                <>
                  <Sunny
                    color={"#ffae00"}
                    title={"Dark Mode"}
                    height="30px"
                    width="30px"
                  />
                </>
              )}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;

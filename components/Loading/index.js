import React from "react";
import { BallTriangle } from "react-loader-spinner";

const Loading = () => {
  return (
    <>
      <div className="self-center z-40 absolute top-0 right-0 left-0 bottom-0 flex justify-center">
        <div className="self-center flex">
          <BallTriangle
            height={100}
            width={100}
            radius={5}
            color="#ef4444"
            ariaLabel="ball-triangle-loading"
            wrapperClass={{}}
            wrapperStyle=""
            visible={true}
          />
          <BallTriangle
            height={100}
            width={100}
            radius={5}
            color="#84cc16"
            ariaLabel="ball-triangle-loading"
            wrapperClass={{}}
            wrapperStyle=""
            visible={true}
          />
          <BallTriangle
            height={100}
            width={100}
            radius={5}
            color="#3b82f6"
            ariaLabel="ball-triangle-loading"
            wrapperClass={{}}
            wrapperStyle=""
            visible={true}
          />
        </div>
      </div>
      <div className="absolute top-0 bottom-0 left-0 right-0 bg-white opacity-70 blur-2xl z-30"></div>
    </>
  );
};

export default Loading;

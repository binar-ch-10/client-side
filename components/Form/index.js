const Input = ({
  id,
  type,
  placeholder,
  namelabel,
  className,
  minlength,
  classDiv,
  value,
  onChange,
  show,
  defaultvalue,
}) => {
  return (
    <div show={show} className={`${classDiv} pb-3 `}>
      <label htmlFor={id}>{namelabel}</label>
      <br />
      <input
        required
        defaultValue={defaultvalue}
        id={id}
        type={type}
        className={`${className} rounded-md px-2 py-2 w-full  `}
        name={id}
        placeholder={placeholder}
        minLength={minlength}
        value={value}
        onChange={onChange}
      />
    </div>
  );
};

export default Input;

import React from "react";

export default function Modal({ showModal, body, footer, title, onClose }) {
  return (
    <>
      {showModal ? (
        <>
          <div className="modal modal-open overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative modal-middle w-1/3  mx-auto max-w-md">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                  <h3 className="text-3xl font-semibold text-slate-700">
                    {title}
                  </h3>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto text-slate-700">
                  {body}
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end p-6 border-t border-solid text-slate-700 border-slate-200 rounded-b">
                  {footer}
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}

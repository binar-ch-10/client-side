import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: true,
};

export const themeModeSlice = createSlice({
  name: "themeMode",
  initialState,
  reducers: {
    changeMode: (state) => {
      state.value = !state.value;
    },
  },
});

// Action creators are generated for each case reducer function
export const { changeMode } = themeModeSlice.actions;

export default themeModeSlice.reducer;

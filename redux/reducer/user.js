import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: "",
};

export const userSlice = createSlice({
  name: "userData",
  initialState,
  reducers: {
    userData: (state = initialState, actions) => {
      state.value = actions.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { userData } = userSlice.actions;

export default userSlice.reducer;

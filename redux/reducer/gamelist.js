import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  games: [],
};

const gamesSlice = createSlice({
  name: "games",
  initialState,
  reducers: {
    selectAllGames: (state, actions) => {
      state.games = actions.payload;
    },
  },
});

export default gamesSlice.reducer;

const { selectAllGames } = gamesSlice.actions;
export const fetchGames = () => async (dispatch) => {
  try {
    await axios
      .get("https://fsw-24-chap-10.herokuapp.com/gamelist")
      .then((response) => dispatch(selectAllGames(response.data)));
  } catch (e) {
    return console.error(e.message);
  }
};

import { configureStore } from "@reduxjs/toolkit";
import themeModeReducer from "./reducer/mode.js";
import userReducer from "./reducer/user.js";
import gamesReducer from "./reducer/gamelist.js"


export const store = configureStore({
  reducer: {
    themeMode: themeModeReducer,
    userData: userReducer,
    games: gamesReducer,
  },
});

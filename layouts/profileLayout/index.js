import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Loading from "../../components/Loading";
import axios from "axios";
import Cookies from "js-cookie";
import Input from "../../components/Form";
import Link from "next/link";

const ProfileLayout = ({ children }) => {
  const [input, setInput] = useState({
    username: "",
    email: "",
  });
  const [profile, setProfile] = useState(null);
  const mode = useSelector((state) => state.themeMode.value);
  const [isLoading, setIsLoading] = useState(false);
  const [fetch, setFetch] = useState(true);
  const [editUsername, setEditUsername] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const user = await axios.get("http://localhost:8082/profile", {
          headers: { Authorization: `${Cookies.get("token")}` },
        });
        setInput({
          username: user.data.data.username,
          email: user.data.data.email,
        });
        setIsLoading(false);
        setProfile(user.data.data);
      } catch (error) {
        setIsLoading(false);
        if (error.response?.status === 401) {
          alert("not find user!");
        }
      }
    };
    if (fetch) {
      fetchData();
      setFetch(false);
    }
  }, [fetch, setFetch]);

  const handlerChange = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  const handlerEdit = async () => {
    setIsLoading(true);
    try {
      let counterUser = false;
      for (let i = 0; i < input.username.length; i++) {
        if (input.username[i] === " ") {
          counterUser = true;
          break;
        }
      }
      if (counterUser) {
        alert("username cannot accept space");
        setIsLoading(false);
        return;
      }
      const userUpdate = await axios.post(
        "http://localhost:8082/updateprofile",
        {
          username: input.username,
        },
        {
          headers: { Authorization: Cookies.get("token") },
        }
      );
      setIsLoading(false);
      setFetch(true);
      setProfile(userUpdate.data.data);
      setEditUsername(false);
    } catch (error) {
      setIsLoading(false);
      if (error.response?.status === 404) {
        alert("error!");
      }
      alert(error);
    }
  };

  const handlerCancel = () => {
    setEditUsername(false);
    setInput(profile);
  };

  return (
    <div>
      <div className="pt-36 pl-6 sm:pl-12 md:pl-20 lg:pl-24 xl:pl-48 2xl:pl-80">
        <div className="sm:flex mb-14 gap-4">
          <div className="flex gap-4">
            <div className="bg-neutral-focus text-neutral-content rounded-full w-24 sm:w-24 h-24 mb-2 flex justify-center">
              <span className="text-3xl self-center uppercase ">
                {profile?.username[0]}
              </span>
            </div>
            <div
              className={`${
                mode ? "text-white" : "text-slate-700"
              } flex flex-col mb-2 justify-center  `}
            >
              {editUsername === true ? (
                <Input
                  id="username"
                  type="text"
                  value={input.username}
                  onChange={handlerChange}
                  className={`${
                    mode ? "text-slate-700" : "border border-slate-700"
                  }`}
                />
              ) : (
                <h1 className=" text-3xl self-start font-semibold ">
                  {profile?.username}
                </h1>
              )}
              <h2 className=" self-start text-lg">{profile?.email}</h2>
            </div>
          </div>
          <div className="sm:pl-12 md:pl-28 lg:pl-64 xl:pl-96 self-center   ">
            {editUsername === false ? (
              <button
                onClick={() => setEditUsername(true)}
                className="btn self-center w-42 h-12"
              >
                edit username
              </button>
            ) : (
              <div className="flex gap-3">
                <button
                  onClick={handlerCancel}
                  className="btn bg-red-500 border-red-500 self-center w-42 h-12"
                >
                  cancel
                </button>
                <button
                  onClick={handlerEdit}
                  className="btn self-center w-42 h-12"
                >
                  save
                </button>
              </div>
            )}
          </div>
        </div>
        <div className="flex gap-10">
          <Link href={"/profile"}>
            <span
              className={`${
                mode ? "text-white" : "text-slate-700"
              } hover:cursor-pointer hover:border-b hover:border-yellow-400 text-3xl font-semibold flex justify-center w-40 mb-10`}
            >
              Biodata
            </span>
          </Link>
          <Link href={"/profile/score"}>
            <span
              className={`${
                mode ? "text-white" : "text-slate-700"
              } hover:cursor-pointer hover:border-b hover:border-yellow-400 text-3xl font-semibold flex justify-center w-40 mb-10`}
            >
              Score
            </span>
          </Link>
        </div>
        <div>{children}</div>
      </div>
    </div>
  );
};

export default ProfileLayout;

import Link from "next/link";
import Cookies from "js-cookie";
import React, { useState } from "react";
import { Moon, Sunny } from "react-ionicons";
import { useDispatch, useSelector } from "react-redux";
import { changeMode } from "../../redux/reducer/mode";
import { useEffect } from "react";
import { useRouter } from "next/router";

const DefaultLayout = ({ children }) => {
  useEffect(() => {
    setToken(Cookies.get("token"));
  }, []);

  const [responsivNavbar, setResponsivNavbar] = useState(false);
  const router = useRouter();
  const [token, setToken] = useState("");
  const user = useSelector((state) => state.userData.value);
  const mode = useSelector((state) => state.themeMode.value);
  const dispatch = useDispatch();

  const handlerLoginLogout = () => {
    if (token) {
      Cookies.remove("token");
      router.push("/log-in");
      return;
    }
  };

  return (
    <div
      className={`${
        mode ? "bg-black text-white" : "bg-white text-slate-500"
      } w-full h-screen `}
    >
      <div
        className={`${
          mode
            ? "bg-black text-white shadow-md shadow-white"
            : "bg-white text-slate-700 font-semibold shadow-lg "
        } absolute  top-0 right-0 left-0 h-16 flex justify-between px-10`}
      >
        <div className="self-center">
          <Link
            className="lg:hover:border-b lg:hover:border-yellow-500"
            href={"/home"}
          >
            HOME
          </Link>
        </div>
        <div className="self-center">
          <div className="flex gap-10">
            <div className="flex invisible lg:visible xl:visible 2xl:visible gap-5">
              <div className={`${""} flex gap-5`}>
                <Link
                  className="lg:hover:border-b lg:hover:border-yellow-500"
                  href={"/"}
                >
                  Games List
                </Link>
                <Link
                  className="lg:hover:border-b lg:hover:border-yellow-500"
                  href={"/profile"}
                >
                  Profile
                </Link>
              </div>
              <div className="dropdown dropdown-bottom dropdown-end">
                <label
                  tabIndex={0}
                  className="bg-slate-500 px-[15px] py-2 border-2 uppercase border-slate-300 text-slate-100 rounded-full hover:cursor-pointer"
                >
                  {user[0]}
                </label>
                <div
                  tabIndex={0}
                  className={`${
                    mode ? "bg-slate-100 text-black" : "bg-slate-900 text-white"
                  } dropdown-content menu p-2 shadow rounded-box w-52`}
                >
                  <div className="mb-1">Header</div>
                  <hr className={`${mode ? "border border-black" : ""} mb-3`} />
                  <div>
                    <button type="button" onClick={handlerLoginLogout}>
                      Logout
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <button
              onClick={() => setResponsivNavbar(!responsivNavbar)}
              className={`${
                responsivNavbar ? "" : "rotate-90"
              } bg-black text-white block lg:hidden duration-500 px-2 rounded pb-1`}
            >
              |||
            </button>
            <button
              className="hover:pointer"
              onClick={() => dispatch(changeMode())}
            >
              {mode ? (
                <>
                  <Moon
                    color={"#ffae00"}
                    title={"Light Mode"}
                    height="30px"
                    width="30px"
                  />
                </>
              ) : (
                <>
                  <Sunny
                    color={"#ffae00"}
                    title={"Dark Mode"}
                    height="30px"
                    width="30px"
                  />
                </>
              )}
            </button>
          </div>
        </div>
      </div>
      {responsivNavbar ? (
        <>
          <div
            className={`${
              mode ? "bg-black text-white" : "bg-white text-slate-700"
            } pt-20 flex justify-end pr-5 gap-5 pb-2`}
          >
            <Link
              className="self-end hover:cursor-pointer font-semibold"
              href={"/"}
            >
              Games List
            </Link>
            <Link
              className="self-end hover:cursor-pointer font-semibold"
              href={"/profile"}
            >
              Profile
            </Link>
          </div>
        </>
      ) : null}

      <div>{children}</div>
    </div>
  );
};

export default DefaultLayout;

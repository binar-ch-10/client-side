import Image from "next/image";
import React, { useEffect, useState } from "react";
import spone from "../../assets/img/spon.webp";
import DefaultLayout from "../../layouts/defaultLayout";
import ProfileLayout from "../../layouts/profileLayout";
import { useSelector } from "react-redux";
import axios from "axios";
import Cookies from "js-cookie";
import Input from "../../components/Form";
import Modal from "../../components/Modal-General";
import Loading from "../../components/Loading";
import moment from 'moment'

const profile = () => {
  const [input, setInput] = useState({
    name: "",
    address: "",
    date_of_birth: "",
    gender: "",
  });
  const [profile, setProfile] = useState(null);
  const mode = useSelector((state) => state.themeMode.value);
  const [isLoading, setIsLoading] = useState(false);
  const [fetch, setFetch] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [isBiodata, setIsBiodata] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const user = await axios.get("http://localhost:8082/profile", {
          headers: { Authorization: `${Cookies.get("token")}` },
        });
        const dataUser = user.data.data;
        if (
          dataUser.name &&
          dataUser.gender &&
          dataUser.address &&
          dataUser.date_of_birth
        ) {
          setIsBiodata(true);
        }
        setInput({
          name: user.data.data.name,
          gender: user.data.data.gender,
          address: user.data.data.address,
          date_of_birth: user.data.data.date_of_birth,
        });
        setIsLoading(false);
        setProfile(user.data.data);
      } catch (error) {
        setIsLoading(false);
        if (error.response?.status === 401) {
          alert("not find user!");
        }
      }
    };
    if (fetch) {
      fetchData();
      setFetch(false);
    }
  }, [fetch, setFetch]);

  const handlerChange = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };

  const handlerSubmitBiodata = async (e) => {
    setIsLoading(true);
    try {
      e.preventDefault();
      setShowModal(false);
      const biodata = await axios.post(
        "http://localhost:8082/createbiodata",
        input,
        {
          headers: { Authorization: Cookies.get("token") },
        }
      );
      setProfile(biodata.data.data);
      setIsLoading(false);
      setFetch(true);
      Router.push("/profile");
    } catch (error) {
      setIsLoading(false);
      if (error.response?.status === 404) {
        alert("User not find!");
      }
    }
  };

  const handleCancelBiodata = () => {
    setShowModal(false);
    setInput(profile);
  };

  return (
    <DefaultLayout>
      <ProfileLayout>
        {isLoading && <Loading />}
        <Modal
          body={
            <div>
              <Input
                id="name"
                type="text"
                placeholder="fullname"
                className="border border-slate-800"
                namelabel="Name"
                defaultvalue={input.name}
                onChange={handlerChange}
              />
              <Input
                id="address"
                type="text"
                placeholder="address"
                className="border border-slate-800"
                namelabel="Address"
                defaultvalue={input.address}
                onChange={handlerChange}
              />
              <div className="pt-4 pb-2 ">
                <select
                  className="border border-slate-800 text-lg text-center rounded-sm my-15 w-3/12"
                  defaultValue={input.gender}
                  onChange={handlerChange}
                  name="gender"
                >
                  <option defaultValue={""}>Gender</option>
                  <option defaultValue={"Male"}>Male</option>
                  <option defaultValue={"Female"}>Female</option>
                </select>
              </div>
              <Input
                id="date_of_birth"
                type="date"
                placeholder="your birth"
                className="border border-slate-800"
                namelabel="Birth"
                defaultvalue={moment(input.date_of_birth).format("YYYY-MM-DD")}
                onChange={handlerChange}
              />
            </div>
          }
          title={"Biodata"}
          footer={
            <div className="flex gap-3">
              <button
                onClick={handleCancelBiodata}
                className="bg-red-400 px-3 py-1 rounded hover:bg-red-500 font-semibold"
              >
                cancel
              </button>
              <button
                onClick={handlerSubmitBiodata}
                className="bg-green-400 px-3 py-1 rounded hover:bg-green-500 font-semibold"
              >
                save
              </button>
            </div>
          }
          onClose={() => setShowModal(false)}
          showModal={showModal}
        />
        {!isBiodata ? (
          <div>
            <div className=" sm:pl-7 md:pl-20 lg:pl-52 xl:pl-80">
              <Image
                className="opacity-50 content-center pb-5"
                src={spone}
                alt=""
                width={400}
                height={400}
              />
              <p
                className={`${
                  mode ? "text-white" : "text-slate-700"
                } pl-16 opacity-50 text-lg font-serif font-semibold`}
              >
                Biodata masih belum ditemukan!!!
              </p>
            </div>
            <button
              onClick={() => setShowModal(true)}
              className="btn self-center w-44 h-12"
            >
              create biodata
            </button>
          </div>
        ) : (
          <div>
            <div
              className={`${
                mode ? "text-white" : "text-slate-700"
              } flex flex-col gap-6 text-xl font-semibold opacity-90}`}
            >
              <p>Name : {profile?.name}</p>
              <p>Address : {profile?.address}</p>
              <p>Gender : {profile?.gender}</p>
              <p>Birth : {moment(profile?.date_of_birth).format("DD-MMM-YYYY")}</p>
            </div>
            <div className="pt-5">
              <button
                onClick={() => setShowModal(true)}
                className="btn self-center w-40 h-12"
              >
                update biodata
              </button>
            </div>
          </div>
        )}
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default profile;

import React, { useEffect, useState } from "react";
import DefaultLayout from "../../layouts/defaultLayout";
import ProfileLayout from "../../layouts/profileLayout";
import { useSelector } from "react-redux";
import axios from "axios";
import Cookies from "js-cookie";
import Loading from "../../components/Loading";

const profile = () => {
  const [scores, setScores] = useState(null);
  const mode = useSelector((state) => state.themeMode.value);
  const [isLoading, setIsLoading] = useState(false);
  const [fetch, setFetch] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const user = await axios.get("http://localhost:8082/scoreprofile", {
          headers: { Authorization: `${Cookies.get("token")}` },
        });
        setIsLoading(false);
        setScores(user.data.data);
      } catch (error) {
        setIsLoading(false);
        if (error.response?.status === 401) {
          alert("not find user!");
        }
        error;
      }
    };
    if (fetch) {
      fetchData();
      setFetch(false);
    }
  }, [fetch, setFetch]);

  return (
    <DefaultLayout>
      <ProfileLayout>
        <div
          className={`${
            mode ? "text-white" : "text-slate-700"
          } flex flex-col gap-6 text-xl font-semibold opacity-90}`}
        >
          {isLoading && <Loading />}
          {scores?.map((scores) => (
            <div>
              {scores.Game.name} : {scores.score}
            </div>
          ))}
        </div>
      </ProfileLayout>
    </DefaultLayout>
  );
};

export default profile;

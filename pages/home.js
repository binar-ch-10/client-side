import React from "react";
import { useSelector } from "react-redux";
import { useState } from "react";
import { useEffect } from "react";
import DefaultLayout from "../layouts/defaultLayout";

const Home = () => {
  const mode = useSelector((state) => state.themeMode.value);
  const [info, setInfo] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const getInfos = async () => {
    const infos = await fetch(
      "https://fsw-24-chap-10.herokuapp.com/information"
    );
    const data = await infos.json();
    setInfo(data.data);
    console.log(data);
    setIsLoading(false);
  };
  useEffect(() => {
    getInfos();
  }, []);

  return (
    <DefaultLayout>
      <div className={`${mode ? "mainBackground" : "secondBackground"}`}>
        <div className="flex container mx-auto justify-center">
          <table class="table-fixed">
            <thead>
              <tr>
                <th className="w-56 text-lg h-16 border-b-2 border-black md:w-80">
                  Information
                </th>
                <th className=" text-lg w-5 h-16 border-b-2 border-black md:w-10"></th>
                <th className=" text-lg w-36 h-16 border-b-2 border-black md:w-60">
                  Date Update
                </th>
              </tr>
            </thead>
            <tbody>
              {info.map((info) => (
                <tr>
                  <td className=" break-words border-b-2 h-16 border-slate-500 text-base text-center">
                    {info.description}
                  </td>
                  <td className="break-words border-b-2 h-16 border-slate-500 text-base text-center"></td>
                  <td className="break-words border-b-2 h-16 border-slate-500 text-base text-center">
                    {info.date}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </DefaultLayout>
  );
};

export default Home;

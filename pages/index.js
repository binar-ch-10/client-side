import Cookies from "js-cookie";
import { useRouter } from "next/router";
import Link from "next/link";
import Loading from "../components/Loading";
import { useEffect } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import axios from "axios";
import Navbar from "../components/Navbar";

export default function Landing() {
  const [game, setGame] = useState([]);
  const [token, setToken] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const mode = useSelector((state) => state.themeMode.value);

  const router = useRouter();

  useEffect(() => {
    setToken(Cookies.get("token"));
  }, []);

  useEffect(() => {
    const getGame = async () => {
      const game = await fetch("https://fsw-24-chap-10.herokuapp.com/gamelist");
      const data = await game.json();
      const gameList = data.data;
      if (token) {
        const isPlayedGame = await axios.get(
          "https://fsw-24-chap-10.herokuapp.com/gamelist/played-game",
          {
            headers: { Authorization: token },
          }
        );
        const dataIsPlayedGame = isPlayedGame.data.data;

        dataIsPlayedGame.forEach((itemIsPlayedGame) => {
          gameList.forEach((itemGameList) => {
            if (itemIsPlayedGame.game_id === itemGameList.id) {
              if (!itemGameList.isPlayedGame) {
                itemGameList.status = true;
              } else {
                itemGameList.status = false;
              }
            }
          });
        });
      }

      setGame(gameList);
      setIsLoading(false);
    };

    getGame();
  }, [token]);

  const ROUTE_GAME_NAME = "/detail/[id]";

  return (
    <div className={`${mode ? "mainBackground" : "secondBackground"}`}>
      <Navbar token={token} />
      <div className="flex flex-wrap gap-x-14 gap-y-10 container mx-auto justify-center relative">
        {isLoading && <Loading />}
        {game.map((game) => (
          <div
            key={game.id}
            className="card card-compact w-60 bg-base-100 shadow-xl md:w-64"
          >
            <figure>
              <img
                src={game.image_url}
                className=" h-40 w-full md:h-52"
                alt={game.name}
              />
            </figure>
            <div className="card-body">
              {game?.status ? (
                <h2 className="card-title text-xs bg-emerald-600 max-w-max py-1 rounded absolute left-0 top-1/2 px-2">
                  pernah dimainkan
                </h2>
              ) : null}
              <h2 className="card-title text-base">{game.name}</h2>
              {game.isAlready ? null : (
                <h2 className="card-title text-xs bg-red-600 max-w-max py-1 rounded absolute left-0 top-1/2 px-2">
                  Coming Soon
                </h2>
              )}
              <div className="card-actions justify-end mt-2">
                <button className="btn btn-primary text-sm">
                  <Link
                    href={{
                      pathname: ROUTE_GAME_NAME,
                      query: { id: game.id },
                    }}
                  >
                    Detail
                  </Link>
                </button>
                <button
                  type="button"
                  onClick={() => {
                    if (token && game.isAlready === true) {
                      router.push({
                        pathname: "/games/[plabel]",
                        query: { plabel: game.label },
                      });
                    } else {
                      router.push("/log-in");
                    }
                  }}
                  className="btn btn-primary text-sm"
                  disabled={game.isAlready ? false : true}
                >
                  {" "}
                  Playnow
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

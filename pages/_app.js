import "../styles/globals.css";
import "../styles/rock-paper.css";
import { store } from "../redux/index";
import { Provider, useDispatch } from "react-redux";
import { useEffect } from "react";
import Cookies from "js-cookie";
import axios from "axios";
import { userData } from "../redux/reducer/user";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    const getUser = async () => {
      if (Cookies.get("token")) {
        const user = await axios.get(
          "https://fsw-24-chap-10.herokuapp.com/user",
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );

        store.dispatch(userData(user.data.data.username));
      }
    };

    getUser();
  }, []);
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp;

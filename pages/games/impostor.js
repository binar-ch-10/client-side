import axios from "axios";
import Cookies from "js-cookie";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import Loading from "../../components/Loading";

const Impostor = () => {
  const game_id = 3;
  const [reFetchData, setreFetchData] = useState(true);
  const [isLoding, setIsLoding] = useState(true);

  useEffect(() => {
    const fetch = async () => {
      try {
        const dataScore = await axios.post(
          "https://fsw-24-chap-10.herokuapp.com/games/rock-paper-scissor",
          { game_id },
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );
        setIsLoding(false);
      } catch (error) {
        alert(error);
        setIsLoding(false);
      }
    };

    if (reFetchData) {
      fetch();
      setreFetchData(false);
    }
  }, [reFetchData]);
  return (
    <div className="h-screen bg-black flex text-white justify-center">
      {isLoding && <Loading />}
      <div className="text-white self-center">
        <Link href={"/"} className="bg-white text-black px-2 py-1 rounded">
          Back
        </Link>
        <p className="mt-10">YOU THE REAL IMPOSTOR</p>
      </div>
    </div>
  );
};

export default Impostor;

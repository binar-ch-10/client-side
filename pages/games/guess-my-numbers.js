import React, { useEffect, useState } from "react";
import Head from "next/head";
import axios from "axios";
import Loading from "../../components/Loading";
import Cookies from "js-cookie";
import { useSelector } from "react-redux";
import Link from "next/link";
import { ChevronBackOutline } from "react-ionicons";

const GuessMyNumbers = () => {
  const game_id = 2;
  const [input, setInput] = useState(0);
  const [secretNumber, setSecretNumber] = useState(null);
  const [result, setResult] = useState("Start guessing...");
  const [scoreInGame, setScoreInGame] = useState(200);
  const [isWin, setIsWin] = useState(false);
  const [isLose, setIsLose] = useState(false);
  const [reFetchData, setreFetchData] = useState(true);
  const [higherScore, setHigherScore] = useState(null);
  const [isLoding, setIsLoding] = useState(false);

  const user = useSelector((state) => state.userData.value);

  const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  useEffect(() => {
    setSecretNumber(getRandomInt(1, 100));
  }, []);

  useEffect(() => {
    const fetch = async () => {
      try {
        console.log("hahahaaha");
        const dataScore = await axios.post(
          "https://fsw-24-chap-10.herokuapp.com/games/rock-paper-scissor",
          { game_id },
          {
            headers: { Authorization: `${Cookies.get("token")}` },
          }
        );
        setHigherScore(dataScore.data.data.score);
        setIsLoding(false);
      } catch (error) {
        alert(error);
        setIsLoding(false);
      }
    };

    if (reFetchData) {
      fetch();
      setreFetchData(false);
    }
  }, [reFetchData]);

  const handleCheck = () => {
    console.log(secretNumber);
    if (scoreInGame - 10 === 0) {
      setResult("You lose...");
      setIsLose(true);
      setScoreInGame(0);
      return;
    }

    if (input > secretNumber) {
      setResult("📈 To hight...");
      setScoreInGame(scoreInGame - 10);
    } else if (input < secretNumber) {
      setResult("📉 To low...");
      setScoreInGame(scoreInGame - 10);
    } else if (input == secretNumber) {
      setResult("🎉 You winn...");
      setIsWin(true);
      if (scoreInGame > higherScore) {
        setIsLoding(true);
        try {
          const postData = async () => {
            const newScore = await axios.put(
              "https://fsw-24-chap-10.herokuapp.com/games/rock-paper-scissor/update-score",
              { score: scoreInGame, game_id },
              {
                headers: { Authorization: `${Cookies.get("token")}` },
              }
            );
            setHigherScore(newScore.data.score);
          };
          postData();
          setreFetchData(true);
        } catch (error) {
          alert(error);
          setIsLoding(false);
        }
      }
    }
  };

  const handleAgain = () => {
    setIsLose(false);
    setIsWin(false);
    setResult("Start guessing...");
    setScoreInGame(200);
    setSecretNumber(getRandomInt(1, 100));
    setInput(0);
  };

  return (
    <>
      <Head>
        <title>Guess My Numbers</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
          href="https://fonts.googleapis.com/css?family=Press+Start+2P&display=swap"
          rel="stylesheet"
        />
      </Head>
      <div
        className={`${
          isWin ? "bg-emerald-500" : "bg-black"
        } container-guess-numbers h-screen`}
      >
        {isLoding ? <Loading /> : null}
        <header
          style={{ height: "36vh" }}
          className={`${
            isWin ? "border-black" : "border-emerald-500"
          } border-b-8 flex justify-between pt-10 lg:px-7 px-2`}
        >
          <div>
            <div className="self-center">
              <Link href="/">
                <ChevronBackOutline
                  color={"#f59e0b"}
                  title={"back"}
                  height="50px"
                  width="50px"
                />
              </Link>
            </div>
            <button
              onClick={handleAgain}
              className={`${
                isWin || isLose ? "visible" : "invisible"
              } text-[8px] lg:text-xl bg-white text-black px-2 py-1`}
            >
              Again!
            </button>
          </div>
          <div className="self-center flex flex-col gap-9 -mb-20">
            <h1 className="text-xs lg:text-4xl mb-8 text-center">
              Guess My Number!
            </h1>
            <div className="text-center text-4xl -mb-16 bg-slate-500 py-10 lg:w-2/5 w-3/4 self-center">
              {isWin || isLose ? secretNumber : "?"}
            </div>
          </div>
          <p className="text-[8px] lg:text-xl">(Between 1 still 100)</p>
        </header>
        <main
          style={{ height: "62vh" }}
          className="flex justify-between lg:px-32 px-3 gap-10 lg:gap-0"
        >
          <section className="flex flex-col self-center">
            <input
              type="number"
              value={input}
              onChange={(e) => setInput(e.target.value)}
              className="border-4 border-white text-xl lg:p-10 p-2 lg:w-60 w-32 text-center block mb-12"
            />
            <button
              onClick={handleCheck}
              disabled={scoreInGame < 10 || isLose || isWin}
              className="btn bg-white text-black hover:bg-slate-200 py-3"
            >
              Check!
            </button>
          </section>
          <section className="self-center">
            <p className="mb-10 text-[8px] lg:text-xl">{result}</p>
            <p className="mb-4 text-[8px] lg:text-xl">
              😉 <span className="score">{user}</span>
            </p>
            <p className="mb-4 text-[8px] lg:text-xl">
              💯 Score in game: <span className="score">{scoreInGame}</span>
            </p>
            <p className="text-[8px] lg:text-xl">
              🥇 Highscore: <span className="highscore">{higherScore}</span>
            </p>
          </section>
        </main>
      </div>
    </>
  );
};

export default GuessMyNumbers;

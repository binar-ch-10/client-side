import Cookies from "js-cookie";
import { useRouter } from "next/router";
import Link from "next/link";
import Loading from "../../components/Loading";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Navbar from "../../components/Navbar";

export const getStaticPaths = async () => {
  const res = await fetch("https://fsw-24-chap-10.herokuapp.com/gamelist");
  const data = await res.json();
  const paths = data.data.map((game) => {
    return {
      params: { id: game.id.toString() },
    };
  });
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async (context) => {
  const id = context.params.id;
  const res = await fetch(
    "https://fsw-24-chap-10.herokuapp.com/gamelist/detail/" + id
  );
  const paths = await res.json();

  return {
    props: { load: paths.data },
  };
};

const Details = ({ load }) => {
  const [token, setToken] = useState(null);

  const user = useSelector((state) => state.userData.value);
  const router = useRouter();

  useEffect(() => {
    setToken(Cookies.get("token"));
  });

  return (
    <div className="mainBackground">
      <Navbar token={token} />
      <div className="flex container mx-auto justify-center md:justify-end">
        <div className="border-solid border-2 border-orange-800 rounded-lg p-5 bg-orange-800">
          <div className="flex-1 w-64  ">
            <img src={load.image_url}></img>
            <br></br>
            <p className="text-lg font-bold text-center"> Name Game</p>
            <p className="text-lg text-center"> {load.name}</p>
            <br></br>
            <p className="text-lg font-bold text-center"> Description Game</p>
            <p className="text-lg text-center"> {load.description}</p>
            <br></br>
            <p className="text-lg font-bold text-center"> Genre Game</p>
            <p className="text-lg text-center"> {load.genre}</p>
            <br></br>
            <button
              className="btn btn-primary justify-items-center"
              onClick={() => {
                if (token && load.isAlready === true) {
                  router.push({
                    pathname: "/games/[plabel]",
                    query: { plabel: load.label },
                  });
                }
                if (token && load.isAlready === false) {
                  router.push("/not-avaliable");
                } else {
                  router.push("/log-in");
                }
              }}
            >
              Playnow
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Details;

import Input from "../components/Form";
import { ChevronBackOutline } from "react-ionicons";
import { useEffect, useState } from "react";
import Loading from "../components/Loading";
import { useRouter } from "next/router";
import axios from "axios";
import Link from "next/link";

const register = () => {
  const [input, setInput] = useState({
    username: "",
    verified: "",
    email: "",
    password: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  const router = useRouter();
  const handlerChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handlerSubmitRegis = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      for (let i = 0; i < input.username.length; i++) {
        if (input.username[i] === " ") {
          alert("username cannot accept space");
          error;
        }
      }
      const user = await axios.post(
        "https://fsw-24-chap-10.herokuapp.com/register",
        input
      );
      setInput({
        username: "",
        verified: "",
        email: "",
        password: "",
      });
      router.push("/log-in");
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      if (error.response?.status === 402) {
        alert("Username already exist!");
      }
      if (error.response?.status === 401) {
        alert("email already exist!");
      }
    }
  };

  return (
    <div className="h-screen lg:flex lg:justify-center regisBackground">
      <div className="max-w-md  min-h-min px-10 mx-auto my-10 lg:self-center pb-10 pt-6 bg-slate-500 bg-opacity-90 rounded-2xl shadow-xl ">
        {isLoading && <Loading />}
        <form
          className=""
          onSubmit={handlerSubmitRegis}
          method="POST"
          action="post"
        >
          <Link href={"/log-in"}>
            <ChevronBackOutline
              color={"#00000"}
              title="back"
              height="30px"
              width="30px"
            />
          </Link>
          <h1 className="text-center text-2xl font-bold pb-5">Register</h1>
          <div className="flex">
            <div className="grid grid-cols-1 w-full bottom-3">
              <Input
                id="email"
                type="email"
                placeholder="name@example.com"
                namelabel="Email"
                value={input.email}
                onChange={handlerChange}
              />
              <Input
                id="password"
                type="password"
                placeholder="password (min 8 char)"
                namelabel="Password"
                minlength="8"
                value={input.password}
                onChange={handlerChange}
              />
              <Input
                id="username"
                type="text"
                placeholder="username (no space)"
                namelabel="Username"
                value={input.username}
                onChange={handlerChange}
              />
              <Input
                id="verified"
                type="text"
                placeholder="unique name"
                namelabel="Verified"
                classDiv="pb-1"
                value={input.verified}
                onChange={handlerChange}
              />
              <p>this form is for verification of forgot password</p>
              <button
                type="submit"
                className="btn mt-5 me-5 hover:bg-black text-white bg-primary"
              >
                Register
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default register;

import Link from "next/link";
import { useEffect, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { GameControllerOutline, ChevronBackOutline } from "react-ionicons";
import Loading from "../components/Loading";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { userData } from "../redux/reducer/user";

const Login = () => {
  const [textHeader, setTextHeader] = useState("Life's Good!");
  const [isLoading, setIsLoading] = useState(false);
  const [input, setInput] = useState({
    email: "",
    password: "",
  });
  const dispatch = useDispatch();
  const route = useRouter();
  const params = useRouter().pathname;

  const handleChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const user = await axios.post(
        "https://fsw-24-chap-10.herokuapp.com/login",
        input
      );
      console.log(user.data.name);
      dispatch(userData(user.data.username));
      Cookies.set("token", user.data.accessToken);
      setInput({
        email: "",
        password: "",
      });
      route.push("/home");
      setIsLoading(false);
    } catch (error) {
      if (error.response?.status === 402) {
        setTextHeader("Email Incorrect!");
        setIsLoading(false);
      }
      if (error.response?.status === 401) {
        setTextHeader("Password Incorrect!");
        setIsLoading(false);
      }
    }
  };

  return (
    <div className="loginBackground h-screen text-red-500">
      {isLoading && <Loading />}
      <div className="flex min-h-screen  lg:justify-end justify-center lg:pr-48 pt-20 lg:pt-0">
        <form
          onSubmit={handleSubmit}
          method="POST"
          className="rounded h-96 lg:self-center flex flex-col lg:w-1/3 w-4/5 p-5 lg:p-10 lg:pt-3 gap-5 shadow-2xl shadow-red-500"
        >
          <Link
            href={"/"}
            className="w-9 h-9 p-0 lg:-ml-8 bg-black rounded-full"
          >
            <ChevronBackOutline
              color={"#f59e0b"}
              title={"back"}
              height="30px"
              width="30px"
            />
          </Link>
          <div className="text-lg font-bold">
            <p className="flex justify-center">
              {textHeader} &nbsp;
              <GameControllerOutline
                color={"#ef4444"}
                beat
                title={"Good Day"}
                height="30px"
                width="20px"
              />
            </p>
          </div>
          <div>
            <input
              required
              className="w-full px-3 rounded py-2 text-yellow-500"
              type="email"
              onChange={handleChange}
              name="email"
              value={input.email}
              placeholder="Email"
            />
          </div>
          <div>
            <input
              required
              className="w-full px-3 rounded py-2 text-yellow-500"
              type="password"
              onChange={handleChange}
              name="password"
              value={input.password}
              placeholder="Password"
            />
          </div>
          <button className="shadow-current shadow-xl px-3 py-2 rounded hover:bg-red-500 hover:text-black font-bold mb-5">
            Log in
          </button>
          <div className="flex flex-col gap-4">
            <p className="text-xs text-white text-end">
              Not registered?{" "}
              <Link className="text-blue-400 italic" href={"/register"}>
                Sign up
              </Link>
            </p>
            <Link className="text-xs text-blue-400 italic text-end" href={""}>
              forgot password?
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
